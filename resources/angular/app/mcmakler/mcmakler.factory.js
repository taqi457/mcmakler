(function () {
    'use strict';

    angular.module('mcmakler')
        .factory('api', api);

    function api($resource, key, $q) {

        var geoLocator = $resource("https://maps.googleapis.com/maps/api/geocode/json?address=:address" + "&key=" + key.API_KEY);

        function getAddress(street, houseNumber, postalCode, city) {
            var result;
            var defer = $q.defer();
            return geoLocator.get({
                address: houseNumber + ' ' + street + ' ' + postalCode + ' ' + city
            }).$promise.then(function (response) {
                var result = {};
                console.log(response, "in factory");
                if (response.status == 'ZERO_RESULTS') {
                    result.success = false;
                } else {
                    result.success = true;
                    result.location = {};
                    result.location.lat = response.results[0].geometry.location.lat;
                    result.location.lng = response.results[0].geometry.location.lng;
                }
                return result;
            });

        }

        return {
            getAddress: getAddress
        };
    }

})();