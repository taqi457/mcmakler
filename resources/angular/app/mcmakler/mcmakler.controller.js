(function () {
    'use strict';

    angular.module('mcmakler')
        .controller('TestCtrl', testController);

    function testController($scope, api) {
        $scope.submit = function () {
            var street = $scope.street;
            var houseNumber = $scope.housenumber;
            var postalCode = $scope.postalcode;
            var city = $scope.city;
            console.log(street, houseNumber, postalCode, city);

            var result = api.getAddress(houseNumber, street, postalCode, city).then(function (result) {
                $scope.result = result;
                console.log($scope.result);
            });

        }

    }

})();